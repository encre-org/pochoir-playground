import { basicSetup } from "codemirror";
import { EditorView, keymap } from "@codemirror/view";
import { html, htmlLanguage } from "@codemirror/lang-html";
import { json, jsonLanguage } from "@codemirror/lang-json";
import { completeFromList } from "@codemirror/autocomplete";
import { indentWithTab } from "@codemirror/commands";
import { oneDark } from "@codemirror/theme-one-dark";
import { Compartment } from "@codemirror/state";

if (typeof WebAssembly === "object") {
  const preview = document.getElementById("preview");
  const templateEditor = document.getElementById("template-editor");
  const contextEditor = document.getElementById("context-editor");
  const switchThemeButton = document.getElementById("switch-theme-button");
  const shareLink = document.getElementById("share-link");
  const spinner = document.getElementById("spinner");
  const focusMessage = document.getElementById("focus-message");
  const lightTheme = EditorView.theme();

  import("./wasm/pochoir_playground.js").then(({ default: init, compile }) => {
    // Register event listeners
    let editorTheme = new Compartment();

    switchThemeButton.addEventListener("click", () => {
      if (document.documentElement.classList.contains("dark")) {
        document.documentElement.classList.remove("dark");
        switchThemeButton.children[0].classList.remove("hidden");
        switchThemeButton.children[1].classList.add("hidden");

        window.templateView.dispatch({
          effects: editorTheme.reconfigure(lightTheme),
        });
        window.contextView.dispatch({
          effects: editorTheme.reconfigure(lightTheme),
        });
      } else {
        document.documentElement.classList.add("dark");
        switchThemeButton.children[0].classList.add("hidden");
        switchThemeButton.children[1].classList.remove("hidden");

        window.templateView.dispatch({
          effects: editorTheme.reconfigure(oneDark),
        });
        window.contextView.dispatch({
          effects: editorTheme.reconfigure(oneDark),
        });
      }
    });

    // Show custom focus shortcut message
    let focusMessageListener = (e) => {
      if (e.key === "Tab" && e.target?.classList?.contains("cm-content")) {
        focusMessage.classList.add("show");
        document.removeEventListener("keyup", focusMessageListener);

        setTimeout(() => focusMessage.classList.remove("show"), 5000);
      }
    });
    document.addEventListener("keyup", focusMessageListener);

    // Hide parts of the page when embedded mode is activated
    const embed = new URLSearchParams(location.search).get("embed");

    if (embed !== null) {
      // Hide the toolbar
      document.querySelector("nav").remove();
    }

    // Initialize the editor
    const updateListenerExtension = EditorView.updateListener.of((update) => {
      if (update.docChanged) {
        updatePreview();
      }
    });

    const code = new URLSearchParams(location.search).get("code");
    const context = new URLSearchParams(location.search).get("context");

    window.templateView = new EditorView({
      doc: code != null && code != "" ? window.decodeURIComponent(code) : `{# Write templating code here #}\n<h1>Hello world!</h1>`,
      extensions:[
        basicSetup,
        keymap.of([indentWithTab]),
        html(),
        htmlLanguage.data.of({}),
        editorTheme.of(lightTheme),
        updateListenerExtension,
      ],
      parent: templateEditor,
    });


    // Hide the context editor if not requested in the embed mode
    if (embed === null || embed.includes("context")) {
      window.contextView = new EditorView({
        doc: context != null && context != "" ? window.decodeURIComponent(context) : `{ "info": "Write the JSON context here" }`,
        extensions:[
          basicSetup,
          keymap.of([indentWithTab]),
          json(),
          jsonLanguage.data.of({}),
          editorTheme.of(lightTheme),
          updateListenerExtension,
        ],
        parent: contextEditor,
      });
    } else {
      contextEditor.remove();
      templateEditor.classList.add("!h-full");
    }

    const updatePreview = () => {
      const html = window.templateView.state.doc.toString();
      const context = window.contextView?.state?.doc?.toString() || window.decodeURIComponent(new URLSearchParams(location.search).get("context")) || `{ "info": "Write the JSON context here" }`;

      const compiled = compile(html, context);

      const previewDoc = preview.contentDocument || preview.contentWindow.document;
      previewDoc.open();
      previewDoc.write(compiled);
      previewDoc.close();

      // Encode twice because changing the link href will decode it once
      shareLink.setAttribute("href", `${location.origin}${location.pathname}?code=${window.encodeURIComponent(window.encodeURIComponent(html))}&context=${window.encodeURIComponent(window.encodeURIComponent(context))}`);
    };

    // Initialize the compiler
    init().then(() => {
      updatePreview();
      preview.classList.remove("hidden");
      spinner.remove();
    });
  });
}
