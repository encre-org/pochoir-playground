<div align="center">
  <h1>pochoir playground</h1>
  <p>A real-time visual playground to mess with `pochoir`'s templating syntax.</p>

  <a href="https://gitlab.com/encre-org/pochoir-playground/blob/main/LICENSE">
    <img alt="MIT License" src="https://img.shields.io/badge/license-MIT-success" />
  </a>

  <a href="https://gitlab.com/encre-org/pochoir-playground/-/pipelines">
    <img alt="Pipeline status" src="https://gitlab.com/encre-org/pochoir-playground/badges/main/pipeline.svg" />
  </a>

  <a href="https://deps.rs/repo/gitlab/encre-org/pochoir-playground">
    <img alt="Dependency status" src="https://deps.rs/repo/gitlab/encre-org/pochoir-playground/status.svg" />
  </a>

  <a href="https://encre-org.gitlab.io/pochoir-playground">
    <img alt="Hosted on gitlab.io" src="https://img.shields.io/static/v1?label=host&message=gitlab.io&color=blue" />
  </a>

  <br>
</div>

### How to serve locally

- Install Rust (see [rustup](https://rustup.rs))
- Install NodeJS (see [NodeJS installation](https://nodejs.org/en/download))
- Install Yarn (see [Yarn installation](https://classic.yarnpkg.com/en/docs/install))
- Clone the [Gitlab](https://gitlab.com/encre-org/pochoir-playground.git) repository (run `git clone https://gitlab.com/encre-org/pochoir-playground.git`)
- Run `yarn install && yarn build` (in the `pochoir-playground` directory)
- Serve the content of the `dist` directory using a server of your taste (e.g with Python 3 `python3 -m http.server 5000`).
  Directly opening the index.html file with a web browser won't work due to strict CSP of WebAssembly binaries
