use wasm_bindgen::prelude::*;

use encre_css::Config;
use pochoir::{component_not_found, ComponentFile, Context};
use serde::de::Error;
use serde_json::from_str;

#[wasm_bindgen]
pub fn compile(source: &str, context: &str) -> String {
    console_error_panic_hook::set_once();

    // Deserialize the context
    match from_str(context).and_then(|c: serde_json::Value| {
        Context::from_serialize(c).ok_or(serde_json::Error::custom(
            "failed to make a Context from the serializable value (the context must be an object)",
        ))
    }) {
        Ok(mut context) => {
            // Compile the template
            match pochoir::compile("inline", &mut context, |name| {
                if name == "inline" {
                    Ok(ComponentFile::new_inline(source))
                } else {
                    Err(component_not_found(name))
                }
            }) {
                Ok(compiled_html) => {
                    // Generate the styles
                    let config = Config::default();

                    format!(
                        r#"<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Playground result</title>
    <style>{}</style>
  </head>
  <body>
    {compiled_html}
  </body>
</html>"#,
                        encre_css::generate([compiled_html.as_str()], &config)
                    )
                }
                Err(e) => {
                    format!(
                        r#"<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Playground result</title>
  </head>
  <body>
  {}
  </body>
</html>"#,
                        pochoir::error::display_html_error(&e, source)
                    )
                }
            }
        }
        Err(e) => format!("JSON Context error: {e}"),
    }
}
